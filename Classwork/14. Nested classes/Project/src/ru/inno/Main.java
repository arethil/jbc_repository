package ru.inno;

public class Main {

    public static void main(String[] args) {
        Table table = new Table();

        table.put("Александр", 35);
        table.put("Наталья", 32);
        table.put("Мафин", 10);

        Table.Iterator iterator = table.new Iterator();
        Table.TableEntry element = iterator.next();

        while (element != null) {
            System.out.println(element.key + " " + element.value);
            element = iterator.next();
        }

        Table anotherTable = new Table();

        anotherTable.put("Black Sabbath", 1968);
        anotherTable.put("Rolling Stones", 1962);
        anotherTable.put("Led Zeppelin", 1968);
        anotherTable.put("Iron Maiden", 1975);
        anotherTable.put("Guns n' Roses", 1985);

        Table.Iterator anotherIterator = anotherTable.new Iterator();
        element = anotherIterator.next();

        while (element != null) {
            System.out.println(element.key + " " + element.value);
            element = anotherIterator.next();
        }
    }
}
