package ru.inno;

public class Table {

    private static final int MAX_TABLE_SIZE = 10;
    private TableEntry[] entries;
    private int count;

    public Table() {
        entries = new TableEntry[MAX_TABLE_SIZE];
        count = 0;
    }

    public static class TableEntry {

        String key;
        int value;

        public TableEntry(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public void put(String key, int value) {
        TableEntry tableEntry = new TableEntry(key, value);
        entries[count++] = tableEntry;
    }

    public class Iterator {

        private int current;

        public Iterator() {
            current = 0;
        }

        public TableEntry next() {
            if(current == count) {
                return null;
            }

            TableEntry currentEntry = entries[current++];
            return currentEntry;
        }
    }
}
