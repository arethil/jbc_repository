public class Program2 {

	public static void printFrom1toN(int n) {

		if(n == 1) {
			System.out.println(1);
		} else {
			from1toN(n - 1);
			System.out.println(n);
		}
	}

	public static int digitsSum(int n) {

		if(n < 10)
			return n;
		else
			return n % 10 + digitsSum(n / 10);
	}

	public static int fibonachi(int n) {

		if(n == 1 || n == 2)
			return 1;
		else
			return fibonachi(n - 1) + fibonachi(n - 2);
	}

	public static void main(String[] args) {
		
		printFrom1toN(5);

		System.out.println(digitsSum(643016));
		
		System.out.println(fibonachi(10));
	}
}
