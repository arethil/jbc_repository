import java.util.Scanner;

public class Program7
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		int[] nums = {-36, -7, 0, 5, 9, 13, 25, 46, 54};

		int num = in.nextInt();		
		int n = nums.length;

		int indexOfLeft = 0;
		int indexOfRight = n - 1;
		int indexOfMiddle = indexOfLeft + (indexOfRight - indexOfLeft) / 2;

		boolean isExists = false;

		while(indexOfLeft <= indexOfRight)
		{
			for(int i = 0; i < n; i++)
			{
				if(i == indexOfLeft)
					System.out.print("[");

				if(i == indexOfMiddle)
					System.out.print("*");

				System.out.print(" " + nums[i] + " ");

				if(i == indexOfRight)
					System.out.print("]");
			}

			System.out.println();

			if(nums[indexOfMiddle] < num)
			{
				indexOfLeft = indexOfMiddle + 1;
			}
			else
			{
				if(nums[indexOfMiddle] > num)
				{
					indexOfRight = indexOfMiddle - 1;
				}
				else
				{
					if(nums[indexOfMiddle] == num)
					{
						isExists = true;
						break;
					}
				}
			}

			indexOfMiddle = indexOfLeft + (indexOfRight - indexOfLeft) / 2;
		}

		System.out.println(isExists);

		in.close();
	}
}
