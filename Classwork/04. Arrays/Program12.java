import java.util.Random;
import java.util.Arrays;

public class Program12
{
	public static void main(String[] args)
	{
		Random random = new Random();
		int[][][] nums = new int[random.nextInt(5) + 2][][];

		for(int i = 0; i < nums.length; i++)
		{
			nums[i] = new int[random.nextInt(5) + 2][];

			for(int j = 0; j < nums[i].length; j++)
			{
				nums[i][j] = new int[random.nextInt(5) + 2];

				for(int l = 0; l < nums[i][j].length; l++)
					nums[i][j][l] = random.nextInt(5) + 2;
			}
		}

		for(int i = 0; i < nums.length; i++)
		{
			System.out.println("Array row - matrix number " + i);

			for(int j = 0; j < nums[i].length; j++)
				System.out.println(Arrays.toString(nums[i][j]));
		}
	}
}
