import java.util.Scanner;

public class Program
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int age0 = in.nextInt();
		int age1 = in.nextInt();
		int age2 = in.nextInt();
		int age3 = in.nextInt();
		int age4 = in.nextInt();

		int sum = age0 + age1 + age2 + age3 + age4;
		double avg = sum / 5.0;
		System.out.println(avg);

		in.close();
	}
}
