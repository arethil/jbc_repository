import java.util.Arrays;

public class Program10
{
	public static void main(String[] args)
	{
		int[][] nums =
		{
			{43, 11, -5},
			{-78, 0, 24},
			{30, -6, 17}
		};

		for(int i = 0; i < nums.length; i++)
		{
			/*
			for(int j = 0; j < nums[i].length; j++)
				System.out.print(nums[i][j] + " ");

			System.out.println();
			*/

			System.out.println(Arrays.toString(nums[i]));
		}
	}
}
