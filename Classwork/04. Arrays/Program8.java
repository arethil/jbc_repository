import java.util.Arrays;

public class Program8
{
	public static void main(String[] args)
	{
		int[] a = {1, 2, 3, 4, 5};
		int[] b = a;

		b[3] = 74;
		System.out.println(Arrays.toString(a));
	}
}
