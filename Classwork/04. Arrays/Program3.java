import java.util.Scanner;

public class Program3
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int n = in.nextInt();

		int[] ages = new int[n];

		for(int i = 0; i < n; i++)
			ages[i] = in.nextInt();

		for(int i : ages)
			System.out.print(i + " ");

		System.out.println();

		int sum = 0;

		for(int i = 0; i < n; i++)
			sum += ages[i];

		double avg = (double)sum / n;
		System.out.println(avg);

		in.close();
	}
}
