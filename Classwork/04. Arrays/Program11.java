import java.util.Scanner;
import java.util.Arrays;

public class Program11
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		int[][] nums = new int[3][];

		nums[0] = new int[2];
		nums[1] = new int[4];
		nums[2] = new int[3];		

		for(int i = 0; i < nums.length; i++)
			for(int j = 0; j < nums[i].length; j++)
				nums[i][j] = in.nextInt();

		for(int i = 0; i < nums.length; i++)
			System.out.println(Arrays.toString(nums[i]));

		in.close();
	}
}
