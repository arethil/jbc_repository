import java.util.Arrays;

public class Program6
{
	public static void main(String[] args)
	{
		int[] nums = {25, 9, -36, 54, 0, 13, -7, 46, 5};
		System.out.println(Arrays.toString(nums));
		
		int n = nums.length;
		int min;
		int indexOfMin;
		int temp;
		
		for(int j = 0; j < n; j++)
		{
			min = nums[j];
			indexOfMin = j;
			
			for(int i = j; i < n; i++)
			{
				if(nums[i] < min)
				{
					min = nums[i];
					indexOfMin = i;
				}
			}

			temp = nums[j];
			nums[j] = nums[indexOfMin];
			nums[indexOfMin] = temp;

			System.out.println("Minimum array element: " + min);
			System.out.println("Index of minimum array element: " + indexOfMin);
			System.out.println(Arrays.toString(nums));
		}
	}
}
