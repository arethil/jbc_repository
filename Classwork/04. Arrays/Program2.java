import java.util.Scanner;

public class Program2
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int n = in.nextInt();

		int[] ages = new int[n];
		
		int i = 0;

		while(i < n)
		{
			ages[i] = in.nextInt();
			i++;
		}

		int sum = 0;
		i = 0;

		while(i < n)
		{
			sum += ages[i];
			i++;
		}

		double avg = (double)sum / n;
		System.out.println(avg);

		in.close();
	}
}
