import java.util.Scanner;

public class Program9
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		int nums[][] = new int[3][4];

		// nums.length -> 3
		// nums[i].length - 4

		for(int i = 0; i < nums.length; i++)
			for(int j = 0; j < nums[i].length; j++)
				nums[i][j] = in.nextInt();

		for(int i = 0; i < nums.length; i++)
		{
			for(int j = 0; j < nums[i].length; j++)
				System.out.print(nums[i][j] + " ");

			System.out.println();
		}

		in.close();
	}
}
