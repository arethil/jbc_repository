public class Program4
{
	public static void main(String[] args)
	{
		int[] nums = {25, 9, -36, 54, 0, 13, -7, 46, 5};

		int min = nums[0];
		int max = nums[0];

		for(int i : nums)
			System.out.print(i + " ");

		System.out.println();

		int n = nums.length;

		for(int i = 1; i < n; i++)
		{
			if(nums[i] < min)
				min = nums[i];

			if(nums[i] > max)
				max = nums[i];
		}

		System.out.println("Minimum array element: " + min);
		System.out.println("Maximum array element: " + max);
	}
}
