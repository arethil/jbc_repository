import java.util.Scanner;

public class Program5
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int num = in.nextInt();
		boolean isExists = false;

		int[] nums = {25, 9, -36, 54, 0, 13, -7, 46, 5};

		for(int i : nums)
			System.out.print(i + " ");

		System.out.println();

		int n = nums.length;

		for(int i = 0; i < n && !isExists; i++)
			if(nums[i] == num)
				isExists = true;

		if(isExists)
			System.out.println("Number exists");
		else
			System.out.println("Number doesn't exist");

		in.close();
	}
}
