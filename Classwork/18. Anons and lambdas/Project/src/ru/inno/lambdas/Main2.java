package ru.inno.lambdas;

public class Main2 {

    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

        numbersUtil.process(10, 15, (first, second) -> first + second);
        numbersUtil.process(12, 5, (first, second) -> first * second);
        numbersUtil.process(678, number -> number / 10);

        numbersUtil.printProcessed();
    }
}
