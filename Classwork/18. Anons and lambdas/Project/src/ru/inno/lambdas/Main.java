package ru.inno.lambdas;

public class Main {

    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

        // ProcessFunction function = new LastDigitProcess();
        ProcessFunction function = new ProcessFunction() {
            @Override
            public int process(int number) {
                return number % 10;
            }
        };

        for (int i = 10; i < 16; i++) {
            numbersUtil.process(i, function);
        }

        numbersUtil.printProcessed();

        ProcessFunction digitsSumFunction = number -> {
            int result = 0;

            while(number != 0) {
                result += number % 10;
                number /= 10;
            }

            return result;
        };

        numbersUtil.process(123, digitsSumFunction);
        numbersUtil.process(4567, digitsSumFunction);
        numbersUtil.process(89, digitsSumFunction);

        numbersUtil.printProcessed();
    }
}
