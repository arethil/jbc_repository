package ru.inno.lambdas;

public interface AnotherProcessFunction {

    int process(int first, int second);
}
