package ru.inno.lambdas;

public class LastDigitProcess implements ProcessFunction {

    @Override
    public int process(int number) {
        return number % 10;
    }
}
