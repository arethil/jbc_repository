package ru.inno.lambdas;

public class NumbersUtil {

    private static final int MAX_PROCESSED_NUMBERS_COUNT = 10;
    private int[] processedNumbers;
    private int processedNumbersCount;

    public NumbersUtil() {
        processedNumbers = new int[MAX_PROCESSED_NUMBERS_COUNT];
    }

    public void printProcessed() {
        for (int i = 0; i < processedNumbersCount; i++) {
            System.out.print(processedNumbers[i] + " ");
        }

        System.out.println();
    }

    public void process(int number, ProcessFunction function) {
        if(isNotFull()) {
            int processedNumber = function.process(number);
            saveNumber(processedNumber);
        } else {
            System.err.println("Место для обработки чисел закончилось");
        }
    }

    public void process(int first, int second, AnotherProcessFunction function) {
        if(isNotFull()) {
            int processedNumber = function.process(first, second);
            saveNumber(processedNumber);
        } else {
            System.err.println("Место для обработки чисел закончилось");
        }
    }

    private boolean isNotFull() {
        return processedNumbersCount < MAX_PROCESSED_NUMBERS_COUNT;
    }

    private void saveNumber(int number) {
        processedNumbers[processedNumbersCount] = number;
        processedNumbersCount++;
    }
}
