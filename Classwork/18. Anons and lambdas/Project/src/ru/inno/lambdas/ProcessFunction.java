package ru.inno.lambdas;

public interface ProcessFunction {

    int process(int number);
}
