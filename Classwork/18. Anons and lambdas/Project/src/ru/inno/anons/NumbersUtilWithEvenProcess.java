package ru.inno.anons;

public class NumbersUtilWithEvenProcess extends NumbersUtil {

    @Override
    protected int processNumber(int number) {
        return number % 2;
    }
}
