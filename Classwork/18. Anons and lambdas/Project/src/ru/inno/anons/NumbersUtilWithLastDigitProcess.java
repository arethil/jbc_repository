package ru.inno.anons;

public class NumbersUtilWithLastDigitProcess extends NumbersUtil {

    @Override
    protected int processNumber(int number) {
        return number % 10;
    }
}
