package ru.inno.anons;

public class Main {

    public static void main(String[] args) {
        NumbersUtil evenProcess = new NumbersUtilWithEvenProcess();

        for (int i = 10; i < 16; i++) {
            evenProcess.process(i);
        }

        evenProcess.printProcessed();

        NumbersUtil lastDigitProcess = new NumbersUtilWithLastDigitProcess();

        for (int i = 10; i < 16; i++) {
            lastDigitProcess.process(i);
        }

        lastDigitProcess.printProcessed();

        NumbersUtil digitsSumProcess = new NumbersUtil() {
            @Override
            protected int processNumber(int number) {
                int result = 0;

                while(number != 0) {
                    result += number % 10;
                    number /= 10;
                }

                return result;
            }
        };

        digitsSumProcess.process(123);
        digitsSumProcess.process(4567);
        digitsSumProcess.process(89);

        digitsSumProcess.printProcessed();
    }
}
