public class Driver {

    private String firstName;
    private String lastName;
    private int experience;

    private Bus bus;

    public Driver(String firstName, String lastName, int experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        if(experience >= 0) {
            this.experience = experience;
        } else {
            this.experience = 0;
        }
    }

    public void goToBus(Bus bus) {
        this.bus = bus;
        bus.setDriver(this);
    }

    public void drive() {
        bus.go();
    }
}
