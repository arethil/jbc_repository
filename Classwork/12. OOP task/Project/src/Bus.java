public class Bus {

    private static final int MAX_PLACES_COUNT = 5;

    private int number;
    private String model;
    private boolean isRun;
    private int passengersCount;

    private Driver driver;
    private Passenger[] passengers;

    public Bus(int number, String model) {
        if(number >= 1) {
            this.number = number;
        } else {
            this.number = 1;
        }

        this.model = model;
        this.passengers = new Passenger[MAX_PLACES_COUNT];
    }

    public void setDriver(Driver driver) {
        if(!isRun) {
            this.driver = driver;
        } else {
            System.err.println("Автобус на линии - невозможно сменить водителя");
        }
    }

    public int getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public void go() {
        this.isRun = true;
        System.out.println("Автобус " + number + " модели " + model + " отправился под управлением " + driver.getFirstName());

        for(int i = 0; i < passengersCount; i++) {
            System.out.println("С нами едет " + passengers[i].getName());
        }
    }

    public void stop() {
        this.isRun = false;
    }

    public void addPassenger(Passenger passenger) {
        if(!isRun) {
            if(passengersCount < MAX_PLACES_COUNT) {
                passengers[passengersCount] = passenger;
                passengersCount++;
            } else {
                System.err.println("Мест нет - невозможно пустить пассажира");
            }
        } else {
            System.err.println("Автобус в пути - невозможно пустить пассажира");
        }
    }
}
