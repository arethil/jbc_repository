public class Main {

    public static void main(String[] args) {
        Driver driver_1 = new Driver("Алексей", "Белкин", 12);
        Driver driver_2 = new Driver("Станислав", "Лаптев", 21);

        Passenger pas_1 = new Passenger("Александр");
        Passenger pas_2 = new Passenger("Наталья");
        Passenger pas_3 = new Passenger("Татьяна");
        Passenger pas_4 = new Passenger("Сергей");
        Passenger pas_5 = new Passenger("Анна");
        Passenger pas_6 = new Passenger("Виктор");

        Bus bus_1 = new Bus(1, "Mercedes");

        driver_1.goToBus(bus_1);

        pas_1.goToBus(bus_1);
        pas_2.goToBus(bus_1);
        pas_3.goToBus(bus_1);
        pas_4.goToBus(bus_1);
        pas_5.goToBus(bus_1);
        pas_6.goToBus(bus_1);

        driver_1.drive();

        driver_2.goToBus(bus_1);
    }
}
