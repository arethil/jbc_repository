public class Program
{
	public static void main(String[] args)
	{
		int num = 57;
		boolean isCondition = num % 2 == 0;

		if(isCondition)
			System.out.println("Even");
		else
			System.out.println("Odd");
	}
}
