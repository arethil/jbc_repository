public class Program2
{
	public static void main(String[] args)
	{
		int num = 5496761;
		int digitsSum = 0;
		int lastDigit;

		while(num != 0)
		{
			lastDigit = num % 10;
			digitsSum += lastDigit;
			num /= 10;
		}

		System.out.println("Digits sum: " + digitsSum);
	}
}
