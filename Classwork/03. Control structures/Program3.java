import java.util.Scanner;

public class Program3
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int currentNum = in.nextInt();
		int evensCount = 0;
		int oddsCount = 0;

		while(currentNum != -1)
		{
			if(currentNum % 2 == 0)
				evensCount++;
			else
				oddsCount++;

			currentNum = in.nextInt();
		}

		System.out.println("Evens count: " + evensCount);
		System.out.println("Odds count: " + oddsCount);

		in.close();
	}
}
