package ru.inno;

public class NumbersBuilder {

    private int value;

    public int getValue() {
        return value;
    }

    public NumbersBuilder addValue(int value) {
        this.value += value;
        return this;
    }
}
