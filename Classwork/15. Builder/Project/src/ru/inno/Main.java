package ru.inno;

public class Main {

    public static void main(String[] args) {
        NumbersBuilder numbersBuilder = new NumbersBuilder();
        System.out.println(numbersBuilder.getValue());

        numbersBuilder
                .addValue(10)
                .addValue(15)
                .addValue(20)
                .addValue(25);

        System.out.println(numbersBuilder.getValue());
    }
}