package ru.inno;

public abstract class Human {

    private static final int MIN_AGE = 0;

    protected String name;
    private int age;

    protected int stepsCount;

    public Human(String name, int age) {
        this.name = name;

        if(age < MIN_AGE) {
            this.age = MIN_AGE;
        } else {
            this.age = age;
        }

        stepsCount = 0;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getStepsCount() {
        return stepsCount;
    }

    public void go() {
        System.out.println(name + " сделал 1 шаг");
        stepsCount++;
    }

    public abstract void run();
}
