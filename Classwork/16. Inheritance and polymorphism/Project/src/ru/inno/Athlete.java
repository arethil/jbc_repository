package ru.inno;

public class Athlete extends Human implements SalaryMan {

    private static final int MIN_RANK = 1;

    private int rank;

    public Athlete(String name, int age, int rank) {
        super(name, age);

        if(rank < MIN_RANK) {
            this.rank = MIN_RANK;
        } else {
            this.rank = rank;
        }
    }

    public int getRank() {
        return rank;
    }

    public void run() {
        System.out.println("Атлет " + name + " побежал, сделал 1000 шагов");
        stepsCount += 1000;
    }

    public void getSalary() {
        System.out.println("Набегал на зарплату");
    }
}
