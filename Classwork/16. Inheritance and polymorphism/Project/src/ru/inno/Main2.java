package ru.inno;

public class Main2 {

    public static void main(String[] args) {
        Athlete athlete = new Athlete("Юрий", 22, 5);
        Student student = new Student("Михаил", 20, 4.4);
        Programmer programmer = new Programmer("Илья", 32, 12);

        Human h1 = athlete;
        Human h2 = student;
        Human h3 = programmer;

        h1.run();
        h2.run();
        h3.run();

        h1.go();
    }
}
