package ru.inno;

public class Main4 {

    public static void main(String[] args) {
        // Athlete athlete = new Athlete("Юрий", 22, 5);
        WorkMan w1 = new Student("Михаил", 20, 4.4);
        WorkMan w2 = new Programmer("Илья", 32, 12);

        Work work = new Work();

        work.doWork(new WorkMan[]{w1, w2});
        work.giveSalary(new SalaryMan[]{new Athlete("Юрий", 22, 5),
                new Programmer("Илья", 32, 12)});
    }
}
