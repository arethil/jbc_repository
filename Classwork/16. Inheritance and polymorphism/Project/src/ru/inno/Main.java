package ru.inno;

public class Main {

    public static void main(String[] args) {
        Athlete athlete = new Athlete("Юрий", 22, 5);
        Student student = new Student("Михаил", 20, 4.4);

        athlete.go();
        athlete.run();

        student.go();
        student.go();
        student.run();

        System.out.println(athlete.getStepsCount());
        System.out.println(student.getStepsCount());
    }
}
