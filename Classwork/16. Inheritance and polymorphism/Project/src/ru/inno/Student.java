package ru.inno;

public class Student extends Human implements WorkMan {

    private double averageMark;

    public Student(String name, int age, double averageMark) {
        super(name, age);
        this.averageMark = averageMark;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public void run() {
        System.out.println("Студент " + name + " побежал, сделал 100 шагов");
        stepsCount += 100;
    }

    public void work() {
        System.out.println("Студент работает");
    }
}
