package ru.inno;

public class Main3 {

    public static void main(String[] args) {
        Athlete athlete = new Athlete("Юрий", 22, 5);
        Student student = new Student("Михаил", 20, 4.4);
        Programmer programmer = new Programmer("Илья", 32, 12);

        Stadium stadium = new Stadium();
        Human[] runners = {athlete, student, programmer};

        stadium.start(runners);
    }
}
