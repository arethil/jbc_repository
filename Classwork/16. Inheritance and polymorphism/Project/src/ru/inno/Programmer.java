package ru.inno;

public class Programmer extends Human implements SalaryMan, WorkMan {

    private int experience;

    public Programmer(String name, int age, int experience) {
        super(name, age);
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void run() {
        System.out.println("Программист " + name + " побежал, сделал 50 шагов");
        stepsCount += 50;
    }

    public void work() {
        System.out.println("Программист работает");
        experience++;
    }

    public void getSalary() {
        System.out.println("Зарплата пришла!");
    }
}
