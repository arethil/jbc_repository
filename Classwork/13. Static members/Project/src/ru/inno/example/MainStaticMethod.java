package ru.inno.example;

public class MainStaticMethod {

    public static void main(String[] args) {
        SomeClass object = new SomeClass(64);

        object.someMethod();
        SomeClass.anotherStaticMethod();
    }
}
