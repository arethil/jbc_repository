package ru.inno.example;

public class MainStaticField {

    public static void main(String[] args) {
        System.out.println(SomeClass.a);

        SomeClass obj_1 = new SomeClass(10);
        obj_1.a = 100;
        System.out.println(SomeClass.a);

        SomeClass obj_2 = new SomeClass(15);
        obj_2.a = 200;
        System.out.println(SomeClass.a);

        SomeClass obj_3 = new SomeClass(20);
        obj_3.a = 300;
        System.out.println(SomeClass.a);

        System.out.println(obj_1.b);
        System.out.println(obj_2.b);
        System.out.println(obj_3.b);

        SomeClass.a = 400;

        System.out.println(obj_1.a);
        System.out.println(obj_2.a);
        System.out.println(obj_3.a);
    }
}
