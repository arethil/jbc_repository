package ru.inno.example;

import java.util.Random;

public class SomeClass {

    public static final int CONST = 100;
    public static int a = 50;
    public int b;

    public SomeClass(int b) {
        this.b = b;
    }

    static {
        Random random = new Random();
        a = random.nextInt(1000);
    }

    public void someMethod() {
        System.out.println("Some method");
        System.out.println(a);
        System.out.println(b);
    }

    public static void someStaticMethod() {
        System.out.println("Some static method");
        System.out.println(a);
    }

    public static void anotherStaticMethod() {
        System.out.println("Another static method");
        someStaticMethod();
    }
}
