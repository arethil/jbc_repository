package ru.inno.singleton;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger("Logger");
        Logger anotherLogger = Logger.getLogger("Another logger");

        logger.info("Программа запущена");

        Scanner scanner = new Scanner(System.in);
        logger.info("Создан Scanner для чтения информации");

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        anotherLogger.info("Считано два числа");

        if(b == 0) {
            logger.error("Попытка деления на ноль");
            anotherLogger.info("Программа остановленна");
            return;
        }

        double result = (double)a / b;
        logger.info("Результат получен");
        System.out.println(result);
        anotherLogger.info("Программа завершена");
    }
}
