#include <iostream>

using namespace std;

struct PersonalData 
{
    string login;
    string password;
};

struct User 
{
    PersonalData *personalData;
    double height;
    bool isWorker;
};

void work(User *user)
{
    user->isWorker = true;
}

void relax(User *user) 
{
    user->isWorker = false;
}

void grow(User *user, double value) 
{
    user->height += value;
}

int main()
{
    // marsel - указатель на структуру в памяти
    User *marsel = new User;
    PersonalData *personalData = new PersonalData;

    marsel->personalData = personalData;
    marsel->height = 1.85;
    marsel->isWorker = false;
    marsel->personalData->login = "marsel";
    marsel->personalData->password = "qwerty007";
    
    work(marsel);
    cout << marsel->isWorker << endl;
    cout << marsel->personalData->login << endl;
    
    User *maxim = new User;

    maxim->height = 1.70;
    maxim->isWorker = true;
    
    relax(maxim);    
    cout << maxim->isWorker << endl;
    
    grow(marsel, 0.1);    
    cout << marsel->height << endl;    

    return 0;
}
