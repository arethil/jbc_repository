public class Program {

	public static double f(double x) {

		return Math.pow(x, 2) * Math.pow(Math.sin(x), 3);
	}

	public static double calcIntegral(double x, double y, int n) {

		double integral = 0;
		double h = (y - x) / n;

		for(double i = x; i <= y; i += h) {
			double height = f(i);
			double width = h;
			double area = height * width;
			integral += area;
		}

		return integral;
	}

	public static void main(String[] args) {

		double result = calcIntegral(0, 3, 10000);
		System.out.println(result);
	}
}
