public class Printer {

    public void printHumans(Human[] humans) {
        int number = 1;

        for(int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].name + " - " + humans[i].getAge());
        }
    }

    public void printAges(int[] ages) {
        for(int i = 0; i < ages.length; i++) {
            System.out.println("Возраст " + i + " встречается " + ages[i] + " раз");
        }
    }
}
