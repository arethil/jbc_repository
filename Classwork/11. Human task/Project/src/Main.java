public class Main {

    public static void main(String[] args) {
        HumansGenerator generator = new HumansGenerator();
        HumansStatistic statistic = new HumansStatistic();
        Printer printer = new Printer();

        Human[] humans = generator.generateHumans(100, 65);
        int[] ages = statistic.calcAges(humans, 65);

        printer.printHumans(humans);
        System.out.println("-------------");
        printer.printAges(ages);
    }
}
