public class HumansStatistic {

    public int[] calcAges(Human[] humans, int maxAge) {
        int[] ages = new int[maxAge + 1];

        for(int i = 0; i < humans.length; i++) {
            int currentAge = humans[i].getAge();
            ages[currentAge]++;
        }

        return ages;
    }
}
