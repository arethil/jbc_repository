import java.util.Random;

public class HumansGenerator {

    public String randomName() {
        Random random = new Random();
        char[] name = new char[10];

        for(int i = 0; i < name.length; i++) {
            name[i] = (char)(random.nextInt(91 - 65) + 65);
        }

        return new String(name);
    }

    public Human[] generateHumans(int humansCount, int maxAge) {
        Random random = new Random();
        Human[] humans = new Human[humansCount];

        for(int i = 0; i < humans.length; i++) {
            Human newHuman = new Human(randomName(), random.nextInt(maxAge + 1));
            humans[i] = newHuman;
        }

        return humans;
    }
}
