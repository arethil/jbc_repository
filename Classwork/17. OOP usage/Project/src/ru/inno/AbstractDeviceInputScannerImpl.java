package ru.inno;

import java.util.Scanner;

public abstract class AbstractDeviceInputScannerImpl implements DeviceInput {

    private Scanner scanner;

    public AbstractDeviceInputScannerImpl() {
        scanner = new Scanner(System.in);
    }

    @Override
    public String read() {
        return scanner.nextLine();
    }
}
