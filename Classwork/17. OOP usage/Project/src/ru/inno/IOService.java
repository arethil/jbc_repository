package ru.inno;

public class IOService {

    private DeviceInput deviceInput;
    private DeviceOutput deviceOutput;

    public IOService(DeviceInput deviceInput, DeviceOutput deviceOutput) {
        this.deviceInput = deviceInput;
        this.deviceOutput = deviceOutput;
    }

    public String read() {
        return deviceInput.read();
    }

    public void print(String message) {
        deviceOutput.write(message);
    }

    public void printDevicesInformation() {
        System.out.println(deviceInput.getInformation());
        System.out.println(deviceOutput.getInformation());
    }
}
