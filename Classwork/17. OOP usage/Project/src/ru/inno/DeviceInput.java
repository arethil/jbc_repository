package ru.inno;

public interface DeviceInput extends Device {

    String read();
}
