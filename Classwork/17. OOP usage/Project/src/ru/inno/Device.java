package ru.inno;

public interface Device {

    String getInformation();
}
