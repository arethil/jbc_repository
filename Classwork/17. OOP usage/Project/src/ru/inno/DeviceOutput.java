package ru.inno;

public interface DeviceOutput extends Device {

    void write(String message);
}
