package ru.inno;

public class DeviceOutputStandardImpl implements DeviceOutput {

    @Override
    public String getInformation() {
        return "Реализация устройства вывода с помощью System.out-потока";
    }

    @Override
    public void write(String message) {
        System.out.println(message);
    }
}
