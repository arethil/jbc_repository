package ru.inno;

public class Main2 {

    public static void main(String[] args) {
        DeviceInput scannerDevice = new DeviceInputScannerImpl();
        DeviceInput prefixDevice = new DeviceInputWithPrefix(scannerDevice, "Сообщение");
        DeviceInput timeDevice = new DeviceInputWithTime(prefixDevice);

        DeviceOutput standardOutput = new DeviceOutputStandardImpl();

        IOService service = new IOService(timeDevice, standardOutput);

        service.printDevicesInformation();

        String message = service.read();
        service.print(message);
    }
}
