package ru.inno;

import java.time.LocalTime;

public class DeviceInputWithTime implements DeviceInput {

    private DeviceInput deviceInput;

    public DeviceInputWithTime(DeviceInput deviceInput) {
        this.deviceInput = deviceInput;
    }

    @Override
    public String getInformation() {
        return "Реализация устройства ввода с таймером";
    }

    @Override
    public String read() {
        return LocalTime.now() + ": " + deviceInput.read();
    }
}
