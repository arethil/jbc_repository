package ru.inno;

public class DeviceOutputErrorImpl implements DeviceOutput {

    @Override
    public String getInformation() {
        return "Реализация устройства вывода с помощью System.err-потока";
    }

    @Override
    public void write(String message) {
        System.err.println(message);
    }
}
