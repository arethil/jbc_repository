package ru.inno;

public class DeviceInputWithPrefix implements DeviceInput {

    private DeviceInput deviceInput;
    private String prefix;

    public DeviceInputWithPrefix(DeviceInput deviceInput, String prefix) {
        this.deviceInput = deviceInput;
        this.prefix = prefix;
    }

    @Override
    public String getInformation() {
        return "Реализация устройства ввода с префиксом";
    }

    @Override
    public String read() {
        return prefix + ": " + deviceInput.read();
    }
}
