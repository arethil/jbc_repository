public class Main {
    public static void main(String[] args) {
        User aleks = new User();

        aleks.height = 1.78;
        aleks.isWorker = false;

        aleks.work();
        System.out.println(aleks.isWorker);

        aleks.grow(0.3);
        System.out.println(aleks.height);
    }
}
