public class Program2 {

	public static void main(String[] args) {

		int result = getSumOfDigits(12034);		
		System.out.println(result);

		if(isEven(result))
			System.out.println("Even");
		else
			System.out.println("Odd");
	}

	public static int getSumOfDigits(int num) {

		int sum = 0;

		while(num != 0) {
			sum += num % 10;
			num /= 10;
		}

		return sum;
	}

	public static boolean isEven(int num) {

		return num % 2 == 0;
	}
}
