import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		int x = in.nextInt();
		int y = in.nextInt();

		printInRange(x, y);

		in.close();
	}

	public static void printInRange(int from, int to) {

		if(from > to) {
			System.err.println("Incorrect range");
			return;
		}

		for(int i = from; i <= to; i++)
			printBeauty(i);

		System.out.println();
	}

	public static void printBeauty(int num) {

		System.out.print(num + " | ");
	}
}
