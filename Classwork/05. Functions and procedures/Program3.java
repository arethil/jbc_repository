import java.util.Arrays;

public class Program3 {

	public static void main(String[] args) {

		int x = 0;
		int y = 2;

		System.out.println(x + " " + y);

		swap(x, y);

		System.out.println(x + " " + y);

		int[] nums = {1, 2, 3};

		System.out.println(Arrays.toString(nums));

		swap(nums, x, y);

		System.out.println(Arrays.toString(nums));
	}

	public static void swap(int a, int b) {

		int temp = a;
		a = b;
		b = temp;
	}

	public static void swap(int[] array, int a, int b) {

		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
}
