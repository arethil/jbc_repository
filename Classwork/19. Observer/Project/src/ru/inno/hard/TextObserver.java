package ru.inno.hard;

public interface TextObserver {

    void handleDocument(String document);
}
