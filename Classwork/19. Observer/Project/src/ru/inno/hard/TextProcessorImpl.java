package ru.inno.hard;

public class TextProcessorImpl implements TextProcessor {

    private static final int MAX_OBSERVERS_COUNT = 5;

    private TextObserver[] observers;
    private int observersCount;

    public TextProcessorImpl() {
        observers = new TextObserver[MAX_OBSERVERS_COUNT];
    }

    @Override
    public void addObserver(TextObserver observer) {
        if(observersCount < MAX_OBSERVERS_COUNT) {
            observers[observersCount] = observer;
            observersCount++;
        } else {
            System.err.println("Допустимое количество обработчиков превышено");
        }
    }

    @Override
    public void notifyObservers(String document) {
        for (int i = 0; i < observersCount; i++) {
            observers[i].handleDocument(document);
        }
    }

    @Override
    public void addDocument(String document) {
        System.out.println("Получен документ: " + document);
        notifyObservers(document);
    }
}
