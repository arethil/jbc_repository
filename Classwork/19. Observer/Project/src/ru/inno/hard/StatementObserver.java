package ru.inno.hard;

public class StatementObserver implements TextObserver {

    private static final String SIGN = "Заявление";

    @Override
    public void handleDocument(String document) {
        if(document.contains(SIGN)) {
            System.out.println("Заявление направлено в соответствующий отдел");
        }
    }
}
