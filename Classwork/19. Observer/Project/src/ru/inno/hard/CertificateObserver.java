package ru.inno.hard;

public class CertificateObserver implements TextObserver {

    private static final String SIGN = "Справка";

    @Override
    public void handleDocument(String document) {
        if(document.contains(SIGN)) {
            System.out.println("Справка отправлена заявителю");
        }
    }
}
