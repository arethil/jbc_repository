package ru.inno.hard;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TextProcessor processor = new TextProcessorImpl();

        processor.addObserver(new StatementObserver());
        processor.addObserver(new CertificateObserver());
        processor.addObserver(document -> System.out.println("Документ <" + document + "> был получен в " + LocalDateTime.now().toString()));

        while(true) {
            String document = scanner.nextLine();
            processor.addDocument(document);
        }
    }
}
