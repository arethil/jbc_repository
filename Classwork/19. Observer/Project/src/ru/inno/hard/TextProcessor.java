package ru.inno.hard;

public interface TextProcessor {

    void addObserver(TextObserver observer);

    void notifyObservers(String document);

    void addDocument(String document);
}
