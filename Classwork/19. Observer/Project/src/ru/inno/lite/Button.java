package ru.inno.lite;

public interface Button {

    void onClick(ClickReaction reaction);

    void click();
}
