package ru.inno.lite;

public class ExitButton implements Button {

    private ClickReaction reaction;

    @Override
    public void onClick(ClickReaction reaction) {
        this.reaction = reaction;
    }

    @Override
    public void click() {
        System.out.println("Выход из приложения");
        reaction.handle();
    }
}
