// Реализовать метод бинарного поиска с помощью рекурсии

import java.util.Scanner;
import java.util.Arrays;

public class Homework0402 {

	public static int binarySearch(int[] array, int element) {

		int left = 0;
		int right = array.length - 1;
		int indexOfElement = innerSearch(array, element, left, right);
		return indexOfElement;
	}

	public static int innerSearch(int[] array, int element, int left, int right) {

		if(left > right) {
			return -1;
		} else {
			int middle = left + (right - left) / 2;

			if(element < array[middle])
				return innerSearch(array, element, left, middle - 1);
			else if(element > array[middle])
				return innerSearch(array, element, middle + 1, right);
			else
				return middle;
		}
	}

	public static void printResult(int indexOfElement) {

		if(indexOfElement == -1)
			System.out.println("The element is not found");
		else
			System.out.println("The element has index " + indexOfElement);
	}

	public static void main(String[] args) {
		
		int[] nums = {-56, -37, -15, -4, 0, 9, 22, 35, 41, 64};

		System.out.println(Arrays.toString(nums));
		System.out.println("Please, enter the number");

		Scanner in = new Scanner(System.in);

		int num = in.nextInt();		
		int searchResult = binarySearch(nums, num);

		printResult(searchResult);
	}
}
