// Реализовать Фибоначчи с однократным вызовом рекурсии

import java.util.Scanner;

public class Homework0403 {

	public static int fibonachi(int n) {

		if(n == 0) {
			return 0;
		} else if(n == 1) {
			return 1;
		} else {
			int fib_0 = 0;
			int fib_1 = 1;
			int count = 1;
			return innerCounting(n, count, fib_1, fib_0);
		}
	}

	public static int innerCounting(int n, int count, int fib_1, int fib_0) {

		int fib_n = fib_1 + fib_0;
		count++;

		if(count == n)
			return fib_n;
		else
			return innerCounting(n, count, fib_n, fib_1);
	}

	public static void main(String[] args) {

		System.out.println("Please, enter the number index");
		
		Scanner in = new Scanner(System.in);

		int n = in.nextInt();

		while(n < 0) {
			System.out.println("The index must not be negative");
			n = in.nextInt();
		}

		System.out.println(fibonachi(n));

		in.close();
	}
}
