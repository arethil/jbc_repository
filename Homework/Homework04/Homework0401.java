// Реализовать рекурсивную функцию, проверяющую, является ли число степенью двойки

import java.util.Scanner;

public class Homework0401 {

	public static boolean isPowerOf2(double num) {

		if((num == 1) || (num == 2))
			return true;
		else if(num < 2)
			return false;
		else
			return isPowerOf2(num / 2);
	}

	public static void main(String[] args) {

		System.out.println("Please, enter the positive number");

		Scanner in = new Scanner(System.in);

		double num = in.nextDouble();

		while(num <= 0) {
			System.out.println("Please, enter the POSITIVE number");
			num = in.nextDouble();
		}

		System.out.println("Is number a power of 2?");
		System.out.println(isPowerOf2(num));

		in.close();
	}
}
