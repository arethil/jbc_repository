/*
Реализовать приложение, которое выполняет разворот массива
(массив вводится с клавиатуры - по одному числу в каждой новой строке).
*/

import java.util.Arrays;
import java.util.Scanner;

public class Homework0201
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int count = in.nextInt();

		int[] nums = new int[count];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		System.out.println(Arrays.toString(nums));

		for(int i = 0; i < nums.length / 2; i++)
		{
			int temp = nums[i];
			nums[i] = nums[nums.length - (i + 1)];
			nums[nums.length - (i + 1)] = temp;
		}

		System.out.println(Arrays.toString(nums));

		in.close();
	}
}
