// Реализовать приложение, которое выполняет сортировку массива методом пузырька.

import java.util.Arrays;
import java.util.Scanner;

public class Homework0204
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int count = in.nextInt();

		int[] nums = new int[count];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		System.out.println(Arrays.toString(nums));

		for(int i = nums.length - 1; i > 0; i--)
		{
			for(int j = 0; j < i; j++)
			{
				if(nums[j] > nums[j + 1])
				{
					int temp = nums[j];
					nums[j] = nums[j + 1];
					nums[j + 1] = temp;
				}
			}
		}

		System.out.println(Arrays.toString(nums));

		in.close();
	}
}
