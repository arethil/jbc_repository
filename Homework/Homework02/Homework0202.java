/*
Реализовать приложение, которое меняет местами максимальный и минимальный
элементы массива (массив вводится с клавиатуры - по одному в каждой новой строке).
*/

import java.util.Arrays;
import java.util.Scanner;

public class Homework0202
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int count = in.nextInt();

		int[] nums = new int[count];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		System.out.println(Arrays.toString(nums));

		int min = nums[0];
		int max = nums[0];
		int indexOfMin = 0;
		int indexOfMax = 0;

		for(int i = 1; i < nums.length; i++)
		{
			if(nums[i] < min)
			{
				min = nums[i];
				indexOfMin = i;
			}

			if(nums[i] > max)
			{
				max = nums[i];
				indexOfMax = i;
			}
		}

		int temp = nums[indexOfMin];
		nums[indexOfMin] = nums[indexOfMax];
		nums[indexOfMax] = temp;

		System.out.println(Arrays.toString(nums));

		in.close();
	}
}
