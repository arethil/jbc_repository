/*
Реализовать приложение, которое заполняет двумерный массив m * n
последовательностью чисел “по спирали”.
*/

import java.util.Scanner;
import java.util.Arrays;

public class Homework0205
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int m = in.nextInt();
		int n = in.nextInt();

		int[][] nums = new int[m][n];

		int row = 0;
		int col = 0;
		int num = 0;
		int i = 0;

		int limit = m * n;
		int count = 0;

		for(;;)
		{
			while(col < n - i)
			{
				nums[row][col] = ++num;
				col++;
			}

			if(num >= limit)
				break;

			row++;
			col--;

			while(row < m - i)
			{
				nums[row][col] = ++num;
				row++;
			}

			row--;
			col--;

			while(col >= i)
			{
				nums[row][col] = ++num;
				col--;
			}

			if(num >= limit)
				break;

			row--;
			col++;
			i++;

			while(row >= i)
			{
				nums[row][col] = ++num;
				row--;
			}

			row++;
			col++;
		}

		for(int j = 0; j < nums.length; j++)
			System.out.println(Arrays.toString(nums[j]));

		in.close();
	}
}
