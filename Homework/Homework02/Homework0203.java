// Реализовать приложение, которое выполняет преобразование массива в число.

import java.util.Arrays;
import java.util.Scanner;

public class Homework0203
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int count = in.nextInt();

		int[] nums = new int[count];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		System.out.println(Arrays.toString(nums));

		long num = 0l;

		for(int i = 0; i < nums.length; i++)
		{
			int temp = Math.abs(nums[i]);
			int mult = 1;

			do
			{
				temp /= 10;
				mult *= 10;
			}
			while(temp != 0);

			num = num * mult + Math.abs(nums[i]);
		}

		System.out.println(num);

		in.close();
	}
}
