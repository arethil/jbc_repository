import java.util.Arrays;
import java.util.Random;

public class Channel {

    private String name;
    private int initialCapacity;
    private Program[] programs;
    private int programsCount;

    public Channel(String name, int initialCapacity) {
        this.name = name;

        if(initialCapacity > 0) {
            this.initialCapacity = initialCapacity;
        } else {
            this.initialCapacity = 1;
        }

        programs = new Program[this.initialCapacity];
        programsCount = 0;
    }

    public Channel(String name) {
        this(name, 3);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addProgram(Program program) {
        if(programsCount == initialCapacity) {
            initialCapacity *= 2;
            programs = Arrays.copyOf(programs, initialCapacity);
        }

        programs[programsCount++] = program;
    }

    public void showRandomProgram() {
        if(programsCount > 0) {
            Random random = new Random();
            int number = random.nextInt(programsCount);
            System.out.println(programs[number].getName());
        } else {
            System.out.println("На данном канале пока отсутствуют программы");
        }
    }
}
