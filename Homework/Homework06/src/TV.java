import java.util.Arrays;

public class TV {

    private String model;
    private int initialCapacity;
    private Channel[] channels;
    private int channelsCount;

    public TV(String model, int initialCapacity) {
        this.model = model;

        if(initialCapacity > 0) {
            this.initialCapacity = initialCapacity;
        } else {
            this.initialCapacity = 1;
        }

        channels = new Channel[this.initialCapacity];
        channelsCount = 0;
    }

    public TV(String model) {
        this(model, 3);
    }

    public String getModel() {
        return model;
    }

    public void addChannel(Channel channel) {
        if(channelsCount == initialCapacity) {
            initialCapacity *= 2;
            channels = Arrays.copyOf(channels, initialCapacity);
        }

        channels[channelsCount++] = channel;
    }

    public void showChannel(int number) {
        if((number < 1) || (number > channelsCount)) {
            System.out.println("Выбранный канал не существует");
        } else {
            channels[number - 1].showRandomProgram();
        }
    }
}
