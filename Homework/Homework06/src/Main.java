public class Main {

    public static void main(String[] args) {
        Program pr_1 = new Program("Утреннее шоу");
        Program pr_2 = new Program("Выпуск новостей");
        Program pr_3 = new Program("Ток-шоу");
        Program pr_4 = new Program("Документальный фильм");
        Program pr_5 = new Program("Художественный фильм");
        Program pr_6 = new Program("Мультипликационный фильм");
        Program pr_7 = new Program("Спортивная трансляция");
        Program pr_8 = new Program("Музыкальный чарт");
        Program pr_9 = new Program("Телевикторина");

        Channel ch_1 = new Channel("Первый", 7);
        Channel ch_2 = new Channel("Второй");
        Channel ch_3 = new Channel("Развлекательный", 2);
        Channel ch_4 = new Channel("Позновательный", -1);

        TV tv = new TV("Sony");
        RemoteController controller = new RemoteController(tv);

        ch_1.addProgram(pr_1);
        ch_1.addProgram(pr_2);
        ch_1.addProgram(pr_3);
        ch_1.addProgram(pr_2);

        ch_2.addProgram(pr_6);
        ch_2.addProgram(pr_9);

        ch_3.addProgram(pr_8);
        ch_3.addProgram(pr_7);
        ch_3.addProgram(pr_5);

        ch_4.addProgram(pr_4);

        tv.addChannel(ch_1);
        tv.addChannel(ch_2);
        tv.addChannel(ch_3);
        tv.addChannel(ch_4);

        controller.selectChannel(0);
        controller.selectChannel(1);
        controller.selectChannel(2);
        controller.selectChannel(3);
        controller.selectChannel(4);
        controller.selectChannel(5);
    }
}
