// Реализовать приложение, со следующим набором функций и процедур:

import java.util.Scanner;
import java.util.Arrays;

public class Homework0301 {

	// выводит сумму элементов массива

	public static int getSumOfArrayElements(int[] array) {
		
		int sum = 0;

		for(int i = 0; i < array.length; i++)
			sum += array[i];

		return sum;
	}

	public static void printSumOfArrayElements(int[] array) {

		System.out.println(getSumOfArrayElements(array));
	}

	// выполняет разворот массива

	public static void reverseArray(int[] array) {

		for(int i = 0; i < array.length / 2; i++) {
			int temp = array[i];
			array[i] = array[array.length - (i + 1)];
			array[array.length - (i + 1)] = temp;
		}
	}

	// вычисляет среднее арифметическое элементов массива

	public static double getAverageOfArrayElements(int[] array) {

		int sum = getSumOfArrayElements(array);
		double avg = (double)sum / array.length;
		return avg;
	}

	// меняет местами максимальный и минимальный элементы массива

	public static void swapMinMaxElements(int[] array) {

		int min = array[0];
		int max = array[0];
		int indexOfMin = 0;
		int indexOfMax = 0;

		for(int i = 1; i < array.length; i++) {

			if(array[i] < min) {
				min = array[i];
				indexOfMin = i;
			}

			if(array[i] > max) {
				max = array[i];
				indexOfMax = i;
			}
		}

		int temp = array[indexOfMin];
		array[indexOfMin] = array[indexOfMax];
		array[indexOfMax] = temp;
	}

	// выполняет сортировку массива методом пузырька

	public static void bubbleSort(int[] array) {

		for(int i = array.length - 1; i > 0; i--) {

			for(int j = 0; j < i; j++) {

				if(array[j] > array[j + 1]) {
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}

	// выполняет преобразование массива в число

	public static long convertArrayToNumber(int[] array) {

		long num = 0l;

		for(int i = 0; i < array.length; i++) {
			int temp = Math.abs(array[i]);
			int mult = 1;

			do {
				temp /= 10;
				mult *= 10;
			}
			while(temp != 0);

			num = num * mult + Math.abs(array[i]);
		}

		return num;
	}

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);

		int count = in.nextInt();

		int[] nums = new int[count];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		System.out.println(Arrays.toString(nums));

		printSumOfArrayElements(nums);

		reverseArray(nums);

		System.out.println(Arrays.toString(nums));
		
		System.out.println(getAverageOfArrayElements(nums));

		swapMinMaxElements(nums);

		System.out.println(Arrays.toString(nums));
		
		bubbleSort(nums);

		System.out.println(Arrays.toString(nums));
		
		System.out.println(convertArrayToNumber(nums));

		in.close();
	}
}
