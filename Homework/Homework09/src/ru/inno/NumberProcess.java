package ru.inno;

public interface NumberProcess {

    int process(int number);
}
