package ru.inno;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] numbers = {3060, 5, 26, 0, 104, 1001, 72, 90, 53};
        NumbersAndStringsProcessor processorA = NumbersAndStringsProcessor.builder().numbers(numbers).build();
        System.out.println(Arrays.toString(processorA.getNumbers()));

        NumberProcess reverseNumberProcess = number -> {
            int result = 0;

            while(number != 0) {
                result = result * 10 + number % 10;
                number /= 10;
            }

            return result;
        };
        System.out.println(Arrays.toString(processorA.process(reverseNumberProcess)));

        NumberProcess zerosRemoveProcess = number -> {
            int result = 0;
            int factor = 1;

            while(number != 0) {
                int lastDigit = number % 10;

                if(lastDigit != 0) {
                    result = result + lastDigit * factor;
                    factor *= 10;
                }

                number /= 10;
            }

            return result;
        };
        System.out.println(Arrays.toString(processorA.process(zerosRemoveProcess)));

        NumberProcess oddsReplaceProcess = number -> {
            int result = 0;
            int factor = 1;

            while(number != 0) {
                int lastDigit = number % 10;

                if(lastDigit % 2 != 0) {
                    --lastDigit;
                }

                result = result + lastDigit * factor;
                factor *= 10;
                number /= 10;
            }

            return result;
        };
        System.out.println(Arrays.toString(processorA.process(oddsReplaceProcess)));

        String[] lines = {"Apple iPhone", "Samsung Galaxy S23", "Google Pixel 8"};
        NumbersAndStringsProcessor processorB = NumbersAndStringsProcessor.builder().lines(lines).build();
        System.out.println(Arrays.toString(processorB.getLines()));

        StringProcess reverseLineProcess = line -> {
            char[] charArray = line.toCharArray();

            for(int i = 0; i < charArray.length / 2; i++) {
                char temp = charArray[i];
                charArray[i] = charArray[charArray.length - (i + 1)];
                charArray[charArray.length - (i + 1)] = temp;
            }

            return new String(charArray);
        };
        System.out.println(Arrays.toString(processorB.process(reverseLineProcess)));

        StringProcess digitsRemoveProcess = line -> {
            char[] charArray = line.toCharArray();
            char[] tempArray = new char[charArray.length];
            int count = 0;

            for(int i = 0; i < charArray.length; i++) {
                if(charArray[i] > 47 && charArray[i] < 58) {
                    continue;
                }

                tempArray[count] = charArray[i];
                count++;
            }

            charArray = Arrays.copyOf(tempArray, count);
            return new String(charArray);
        };
        System.out.println(Arrays.toString(processorB.process(digitsRemoveProcess)));

        StringProcess lowerCaseReplaceProcess = line -> {
            char[] charArray = line.toCharArray();

            for(int i = 0; i < charArray.length; i++) {
                if(charArray[i] > 96 && charArray[i] < 123) {
                    charArray[i] -= 32;
                }
            }

            return new String(charArray);
        };
        System.out.println(Arrays.toString(processorB.process(lowerCaseReplaceProcess)));
    }
}
