package ru.inno;

public class NumbersAndStringsProcessor {

    private int[] numbers;
    private String[] lines;

    private NumbersAndStringsProcessor() {}

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private NumbersAndStringsProcessor processor;

        public Builder() {
            processor = new NumbersAndStringsProcessor();
        }

        public Builder numbers(int[] numbers) {
            processor.numbers = numbers;
            return this;
        }

        public Builder lines(String[] lines) {
            processor.lines = lines;
            return this;
        }

        public NumbersAndStringsProcessor build() {
            return processor;
        }
    }

    public int[] process(NumberProcess numberProcess) {
        int[] processedNumbers = new int[numbers.length];

        for(int i = 0; i < numbers.length; i++) {
            processedNumbers[i] = numberProcess.process(numbers[i]);
        }

        return processedNumbers;
    }

    public String[] process(StringProcess lineProcess) {
        String[] processedLines = new String[lines.length];

        for(int i = 0; i < lines.length; i++) {
            processedLines[i] = lineProcess.process(lines[i]);
        }

        return processedLines;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public String[] getLines() {
        return lines;
    }
}
