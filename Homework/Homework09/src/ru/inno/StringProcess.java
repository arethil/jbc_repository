package ru.inno;

public interface StringProcess {

    String process(String line);
}
