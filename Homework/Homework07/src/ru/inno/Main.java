package ru.inno;

public class Main {

    public static void main(String[] args) {
        User user = User.builder()
                .firstName("Александр")
                .age(35)
                .build();

        user.printUserInfo();

        User anotherUser = User.builder()
                .firstName("Ирина")
                .lastName("Тихонова")
                .build();

        anotherUser.printUserInfo();
    }
}
