package ru.inno;

public class User {

    private String firstName;
    private String lastName;
    private int age;

    private User() {}

    public static class Builder {

        private User user;

        public Builder() {
            this.user = new User();
        }

        public Builder firstName(String firstName) {
            this.user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.user.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.user.age = age;
            return this;
        }

        public User build() {
            return this.user;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void printUserInfo() {
        System.out.printf("first name: %s\nlast name: %s\nage: %d\n",
                this.firstName, this.lastName, this.age);
    }
}
