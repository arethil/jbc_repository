package ru.inno;

public abstract class GeometricFigure implements Relocatable, Scalable, Printable {

    protected FigureCenter center;
    protected double sideA;
    protected double sideB;

    public GeometricFigure(double coordinateX, double coordinateY, double sideA, double sideB) {
        center = new FigureCenter(coordinateX, coordinateY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public FigureCenter getCenter() {
        return center;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    @Override
    public void relocateFigure(double coordinateX, double coordinateY) {
        center.setCoordinateX(coordinateX);
        center.setCoordinateY(coordinateY);
    }

    @Override
    public void scaleFigure(double value) {
        sideA *= value;
        sideB *= value;
    }

    @Override
    public void printFigureCenterCoordinates() {
        System.out.println("center coordinates: " + center.getCoordinateX()
                + "; " + center.getCoordinateY());
    }

    @Override
    public void printFigurePerimeter() {
        System.out.println("perimeter P = " + calcPerimeter());
    }

    @Override
    public void printFigureArea() {
        System.out.println("area S = " + calcArea());
    }

    public abstract double calcPerimeter();

    public abstract double calcArea();
}
