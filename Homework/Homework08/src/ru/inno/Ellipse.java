package ru.inno;

public class Ellipse extends GeometricFigure {

    public Ellipse(double coordinateX, double coordinateY, double semiMajorAxis, double semiMinorAxis) {
        super(coordinateX, coordinateY, semiMajorAxis, semiMinorAxis);
    }

    @Override
    public void printFigureName() {
        System.out.println("Ellipse");
    }

    @Override
    public void printFigureSize() {
        System.out.println("semi-major axis a = " + sideA);
        System.out.println("semi-minor axis b = " + sideB);
    }

    @Override
    public double calcPerimeter() {
        return 4 * ((Math.PI * sideA * sideB +
                Math.pow(sideA - sideB, 2)) / (sideA + sideB));
    }

    @Override
    public double calcArea() {
        return Math.PI * sideA * sideB;
    }
}
