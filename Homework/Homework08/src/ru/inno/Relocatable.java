package ru.inno;

public interface Relocatable {

    void relocateFigure(double coordinateX, double coordinateY);
}
