package ru.inno;

public class Main {

    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(6.7, 4, 12, 7.3);
        Circle circle = new Circle(0, 9.75, 10);
        Rectangle rectangle = new Rectangle(-8, 0, 4.6, 14);
        Square square = new Square(-12, 5, 12.2);

        GeometricFigure[] figures = {ellipse, circle, rectangle, square};

        figures[0].relocateFigure(8.67, 6.26);
        figures[3].relocateFigure(4, -8.7);

        figures[1].scaleFigure(2.5);
        figures[2].scaleFigure(0.5);

        for(int i = 0; i < figures.length; i++) {
            figures[i].printFigureInfo();
            System.out.println();
        }
    }
}
