package ru.inno;

public class Square extends Rectangle {

    public Square(double coordinateX, double coordinateY, double sideA) {
        super(coordinateX, coordinateY, sideA, sideA);
    }

    @Override
    public void printFigureName() {
        System.out.println("Square");
    }

    @Override
    public void printFigureSize() {
        System.out.println("side a = " + sideA);
    }
}
