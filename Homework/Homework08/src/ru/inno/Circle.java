package ru.inno;

public class Circle extends Ellipse {

    public Circle(double coordinateX, double coordinateY, double radius) {
        super(coordinateX, coordinateY, radius, radius);
    }

    @Override
    public void printFigureName() {
        System.out.println("Circle");
    }

    @Override
    public void printFigureSize() {
        System.out.println("radius r = " + sideA);
    }

    @Override
    public double calcPerimeter() {
        return 2 * Math.PI * sideA;
    }
}
