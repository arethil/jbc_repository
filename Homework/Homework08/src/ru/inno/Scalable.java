package ru.inno;

public interface Scalable {

    void scaleFigure(double value);
}
