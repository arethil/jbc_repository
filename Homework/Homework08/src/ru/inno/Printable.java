package ru.inno;

public interface Printable {

    void printFigureName();

    void printFigureCenterCoordinates();

    void printFigureSize();

    void printFigurePerimeter();

    void printFigureArea();

    default void printFigureInfo() {
        printFigureName();
        printFigureCenterCoordinates();
        printFigureSize();
        printFigurePerimeter();
        printFigureArea();
    }
}
