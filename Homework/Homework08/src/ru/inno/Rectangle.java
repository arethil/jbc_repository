package ru.inno;

public class Rectangle extends GeometricFigure {

    public Rectangle(double coordinateX, double coordinateY, double sideA, double sideB) {
        super(coordinateX, coordinateY, sideA, sideB);
    }

    @Override
    public void printFigureName() {
        System.out.println("Rectangle");
    }

    @Override
    public void printFigureSize() {
        System.out.println("side a = " + sideA);
        System.out.println("side b = " + sideB);
    }

    @Override
    public double calcPerimeter() {
        return 2 * (sideA + sideB);
    }

    @Override
    public double calcArea() {
        return sideA * sideB;
    }
}
