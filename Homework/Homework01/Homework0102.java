/*
Реализовать приложение, которое выводит двоичное представление пятизначного числа
(значение числа задается непосредственно в коде).
В данном задании запрещено использование циклов и массивов.
*/

public class Homework0102
{
	public static void main(String[] args)
	{
		int number = 16754;
		String binaryDigit;
		String binaryNumber;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);

		number /= 2;

		if(number % 2 == 0)
			binaryDigit = "0";
		else
			binaryDigit = "1";

		binaryNumber = binaryDigit + binaryNumber;
		System.out.println(number + "\t" + binaryDigit);
		System.out.println("16754(10) = " + binaryNumber + "(2)");
	}
}
