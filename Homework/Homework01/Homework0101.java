/*
Реализовать приложение, которое выводит сумму цифр пятизначного числа
(значение числа задается непосредственно в коде).
В данном задании запрещено использование циклов и массивов.
*/

public class Homework0101
{
	public static void main(String[] args)
	{
		int number = 16754;
		int lastDigit = number % 10;
		int digitsSum = lastDigit;

		System.out.println(number);

		number /= 10;
		lastDigit = number % 10;
		digitsSum += lastDigit;

		number /= 10;
		lastDigit = number % 10;
		digitsSum += lastDigit;

		number /= 10;
		lastDigit = number % 10;
		digitsSum += lastDigit;

		number /= 10;
		lastDigit = number % 10;
		digitsSum += lastDigit;
		
		System.out.println("Digits sum: " + digitsSum);		
	}
}
