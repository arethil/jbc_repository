/*
Реализовать приложение, которое для заданной последовательности чисел считает
произведение тех чисел, сумма цифр которых - простое число. Последнее число
последовательности - 0.
*/

import java.util.Scanner;

public class Homework0103
{
	public static void main(String[] args)
	{
		System.out.println("Enter natural number");

		Scanner in = new Scanner(System.in);

		int number = in.nextInt();
		int product = 1;

		while(number != 0)
		{
			if(number < 0)
			{
				System.out.println("Incorrect value, enter natural number");
			}
			else
			{
				int temp = number;
				int digitsSum = 0;
				int lastDigit;

				while(temp != 0)
				{
					lastDigit = temp % 10;
					digitsSum += lastDigit;
					temp /= 10;
				}

				boolean isPrime = true;
				int divisor = 2;

				if(digitsSum == 1)
				{
					isPrime = false;
				}
				else
				{
					while(divisor <= Math.sqrt(digitsSum))
					{
						if(digitsSum % divisor == 0)
						{
							isPrime = false;
							break;
						}

						divisor++;
					}
				}

				System.out.printf("%d(digits sum: %d, prime number: %b)\n", number, digitsSum, isPrime);

				if(isPrime)
					product *= number;
			}

			number = in.nextInt();
		}

		if(product == 1)
			System.out.println("There are no numbers which sum is a prime number.");
		else
			System.out.println("Product of numbers which sum is a prime number: " + product);

		in.close();
	}
}
