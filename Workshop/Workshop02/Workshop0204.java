// Объявить и вывести двумерный массив.

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Workshop0204
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		Random random = new Random();

		int rows = in.nextInt();
		int cols = in.nextInt();

		int[][] nums = new int[rows][cols];

		for(int i = 0; i < nums.length; i++)
			for(int j = 0; j < nums[i].length; j++)
				nums[i][j] = random.nextInt(100);

		for(int i = 0; i < nums.length; i++)
			System.out.println(Arrays.toString(nums[i]));

		in.close();
	}
}
