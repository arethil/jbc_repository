// Продемонстрировать использование массива, как ссылочного типа данных.

import java.util.Arrays;

public class Workshop0202
{
	public static void main(String[] args)
	{
		int[] nums1 = {1, 2, 3, 4, 5, 6, 7};
		int[] nums2 = nums1;
		
		nums2[5] = 65;
		System.out.println(Arrays.toString(nums1));
	}
}
