// Реализовать сортировку выбором и бинарный поиск.

import java.util.Arrays;
import java.util.Scanner;

public class Workshop0203
{
	public static void main(String[] args)
	{
		int[] nums = {16, 0, 87, -30, 54, -9, 6, 23, -65, 22};
		System.out.println(Arrays.toString(nums));

		int n = nums.length;
		int min;
		int indexOfMin;
		int temp;

		for(int j = 0; j < n; j++)
		{
			min = nums[j];
			indexOfMin = j;

			for(int i = j; i < n; i++)
			{
				if(nums[i] < min)
				{
					min = nums[i];
					indexOfMin = i;					
				}
			}

			temp = nums[j];
			nums[j] = nums[indexOfMin];
			nums[indexOfMin] = temp;
		}

		System.out.println(Arrays.toString(nums));
		System.out.print("Enter a number to search: ");

		Scanner in = new Scanner(System.in);

		int num = in.nextInt();
		boolean isExists = false;

		int left = 0;
		int right = n - 1;
		int middle = left + (right - left) / 2;

		while(left <= right)
		{
			for(int i = 0; i < n; i++)
			{
				if(i == left)
					System.out.print("[");

				if(i == middle)
					System.out.print("*");

				System.out.print(" " + nums[i] + " ");

				if(i == right)
					System.out.print("]");
			}

			System.out.println();

			if(num > nums[middle])
			{
				left = middle + 1;
			}
			else
			{
				if(num < nums[middle])
				{
					right = middle - 1;
				}
				else
				{
					isExists = true;
					break;
				}
			}

			middle = left + (right - left) / 2;
		}

		System.out.println(isExists);

		in.close();
	}
}
