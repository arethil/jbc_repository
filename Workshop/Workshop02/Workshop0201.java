// Вывести считанный с консоли массив в обратном порядке.

import java.util.Scanner;

public class Workshop0201
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();

		int[] nums = new int[n];

		for(int i = 0; i < n; i++)
			nums[i] = in.nextInt();

		for(int i = n - 1; i >= 0; i--)
			System.out.print(nums[i] + " ");

		in.close();
	}
}
