/*
Для входной последовательности чисел сформировать двумерный массив,
где на i-ой строке будут размещены числа, оканчивающиеся на цифру i.
*/

import java.util.Scanner;
import java.util.Arrays;

public class Workshop0205
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int limit = in.nextInt();
		int count = 0;

		int[] nums = new int[limit];
		int[][] table = new int[10][];

		for(int i = 0; i < nums.length; i++)
			nums[i] = in.nextInt();

		for(int i = 0; i < table.length; i++)
		{
			for(int j = 0; j < nums.length; j++)
				if(Math.abs(nums[j] % 10) == i)
					count++;

			if(count > 0)
			{
				table[i] = new int[count];

				for(int l = 0; l < nums.length; l++)
				{
					if(Math.abs(nums[l] % 10) == i)
					{
						table[i][table[i].length - count] = nums[l];
						count--;
					}
				}
			}
		}

		for(int i = 0; i < table.length; i++)
			System.out.println(Arrays.toString(table[i]));

		in.close();
	}
}
