// Реализовать приложение, которое выводит “Yes”, если число, заданное в коде - четное.

public class Workshop0102
{
	public static void main(String[] args)
	{
		int number = 76;
		System.out.println(number);

		if(number % 2 == 0)
			System.out.println("Yes");
		else
			System.out.println("No");
	}
}
