// Реализовать приложение, которое выводит сумму цифр произвольного числа.

import java.util.Scanner;

public class Workshop0103
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		int number = in.nextInt();
		int digitsSum = 0;
		int lastDigit;

		System.out.println(number);

		while(number != 0)
		{
			lastDigit = number % 10;
			digitsSum += lastDigit;
			number /= 10;
		}

		System.out.println("Digits sum: " + digitsSum);

		in.close();
	}
}
