// Реализовать приложение, которое выводит сумму цифр семизначного числа.

public class Workshop0101
{
	public static void main(String[] args)
	{
		int number = 5673925;
		int digitsSum = 0;
		int lastDigit;

		System.out.println(number);

		while(number != 0)
		{
			lastDigit = number % 10;
			digitsSum += lastDigit;
			number /= 10;
		}

		System.out.println("Digits sum: " + digitsSum);
	}
}
